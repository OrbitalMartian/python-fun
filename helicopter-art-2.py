import graphics_library

# Create a canvas
canvas = graphics_library.create_canvas(width, height)

# Define helicopter parameters
helicopter_color = "red"
helicopter_shape = "sleek"

# Draw the MD902 helicopter
canvas.draw_helicopter(color=helicopter_color, shape=helicopter_shape, model="MD902")

# Display the canvas
canvas.show()
