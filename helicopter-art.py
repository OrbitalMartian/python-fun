import graphics_library

# Create a canvas
canvas = graphics_library.create_canvas(width, height)

# Define helicopter parameters
helicopter_color = "blue"
helicopter_shape = "sleek"

# Draw the helicopter
canvas.draw_helicopter(color=helicopter_color, shape=helicopter_shape)

# Display the canvas
canvas.show()
